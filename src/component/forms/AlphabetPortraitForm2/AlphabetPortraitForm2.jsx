import React, { Component } from "react";
import styles from "./AlphabetPortraitForm2.module.scss";
import InputMask from "react-input-mask";
import { CardPortrait } from "../../UI/CardPortrait/CardPortrait";
import AlphabetChar from "../../UI/AlphabetChar/AlphabetChar";
import TargetPowerShadow2 from "../../UI/TargetPowerShadow/TargetPowerShadow2";
import ButtonDownloadAndPrint from "../../UI/ButtonDownloadAndPrint/ButtonDownloadAndPrint";
import { getMainArcaneForKhrzanowska } from "../../../Func";
import imgArrowLeftBlack from "../../../img/arrow_left_black.png";
import imgArrowRightBlack from "../../../img/arrow_right_black.png";
import { Link } from "react-router-dom";

class AlphabetPortraitForm2 extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      Name: "",
      Date: "",
    };
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var dataForm = this.props.DataForm;
    var name = dataForm.InputName.Value;
    var date = dataForm.InputDate.Value;
    var isSuccess = !!this.props.IsSuccess;

    var clsRowButtonSubmit = ["row"];
    var clsInputDate = ["form-control", styles.InputForm];
    var clsInputName = ["form-control", styles.InputForm];
    if (!dataForm.InputDate.Validate || !dataForm.InputName.Validate) {
      if (!dataForm.InputDate.Validate) {
        clsInputDate.push("is-invalid");
      }
      if (!dataForm.InputName.Validate) {
        clsInputName.push("is-invalid");
      }
    }

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h3 text-uppercase my-3">Алфавитный Портрет (А. Хшановской)</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <CardPortrait IsBackgroundColor={true}>
              <div className="row">
                <div className="col-12">
                  <div className="d-flex justify-content-center align-items-center">
                    <label className="d-flex flex-column justify-content-center align-items-center">
                      <span className={styles.LabelText}>Имя | Наименование | Слово</span>
                      <input type="text" className={clsInputName.join(" ")} value={name} onChange={(e) => this.props.onChangeName(e)} />
                      <span className={`d-flex justify-content-center align-items-end ${styles.LabelText}`}>
                        <span>Ввод возможен на русском | латинице | +цифры</span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
              {isSuccess ? (
                <div className="row">
                  <div className="col-12">
                    {this.props.NameValues.map((nameValue, indexJ) => {
                      return (
                        <React.Fragment key={indexJ}>
                          <div className="d-flex flex-column my-3">
                            <div className="d-flex justify-content-center align-items-center text-uppercase font-KabelBookTT">{nameValue.ValueName}</div>
                            <div className="d-flex justify-content-center align-items-center">
                              {nameValue.ValuesNumberPos.map((valueNumberPos, index) => {
                                return <AlphabetChar key={index} ValueNumberPosValue={valueNumberPos[0]} ValueNumberPosNumber={valueNumberPos[1]} />;
                              })}
                            </div>
                            <TargetPowerShadow2 MainArcane={nameValue.MainArcane} ValueTarget={nameValue.ValueTarget} ValuePower={nameValue.ValuePower} ValueShadow={nameValue.ValueShadow} />
                          </div>
                        </React.Fragment>
                      );
                    })}
                  </div>
                </div>
              ) : null}
              {!isSuccess ? <div className="my-3"></div> : null}
              <div className="row">
                <div className="col-12">
                  <div className="d-flex justify-content-center align-items-center">
                    <label className="d-flex flex-column">
                      <div className="d-flex justify-content-center">
                        <span>Дата рождения</span>
                        <span className="mx-2">|</span>
                        <span>Основание</span>
                        <span className="mx-2">|</span>
                        <span>Начало</span>
                      </div>
                      <InputMask
                        type={"text"}
                        mask="99.99.9999"
                        className={clsInputDate.join(" ")}
                        name="date"
                        placeholder="ДД.ММ.ГГГГ"
                        value={date}
                        onChange={(e) => this.props.onChangeDate(e)}
                      />
                      <div className="d-flex justify-content-center">
                        <span>День</span>
                        <span className="mx-2">Месяц</span>
                        <span>Год</span>
                      </div>
                    </label>
                  </div>
                </div>
              </div>
              {isSuccess ? (
                <React.Fragment>
                  <div className={clsRowButtonSubmit.join(" ")}>
                    <div className="col-12 d-flex justify-content-center align-items-center">
                      <button className="btn btn-primary btn-lg my-3 px-5 text-light btn-w-250" onClick={(e) => this.props.onChangeAction("AlphabetPortraitAnalysis")}>
                        Открыть
                      </button>
                    </div>
                  </div>
                </React.Fragment>
              ) : null}
              <div className={clsRowButtonSubmit.join(" ")}>
                <div className="col-12">
                  <div className="row pt-3">
                    <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-end align-items-center">
                      <Link to="/calc-numerologist-luna-shafran/">
                        <button className="btn btn-link text-decoration-none ">
                          <span className="me-1">Ввод даты</span>
                          <img src={imgArrowLeftBlack} alt="<" className={styles.ImgIcon} />
                        </button>
                      </Link>
                    </div>
                    <div className="col-12 col-md-4 d-flex justify-content-center align-items-center">
                      <button className="btn btn-primary btn-lg my-3 px-5 text-light btn-w-250" onClick={this.props.handleCalc}>
                        Рассчитать
                      </button>
                    </div>
                    <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-start align-items-center">
                      <Link to="/calc-numerologist-luna-shafran/composite-portrait">
                        <button className="btn btn-link text-decoration-none">
                          <img src={imgArrowRightBlack} alt=">" className={styles.ImgIcon} />
                          <span className="ms-1">Композитный портрет</span>
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </CardPortrait>
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default AlphabetPortraitForm2;
