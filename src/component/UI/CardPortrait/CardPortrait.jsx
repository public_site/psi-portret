import React, { Component } from "react";
import styles from "./CardPortrait.module.scss";
import { MdOutlineCancel } from "react-icons/md";

export const CardPortrait = (props) => {
  var clsWrapper = ["d-flex", "justify-content-center", styles.Wrapper];
  var cls = [styles.CardPortrait];
  var title = !props.Title ? false : props.Title;
  if (!!props.className) {
    cls.push(props.className);
  }
  if (!!props.IsPrint) {
    clsWrapper.push(styles.WrapperPrint);
    cls.push(styles.Print);
  }
  if (!!props.IsBackgroundColor) {
    cls.push(styles.BackgroundColor);
  }

  var clsContainer = [styles.Container];
  var portraitName = !!props.PortraitName ? props.PortraitName : false;

  if (portraitName == "FullIndivid") {
    cls.push(styles.CardPortraitFullIndivid);
    clsContainer.push(styles.ContainerForFullIndivid);
  }
  if (portraitName == "FullIndividDescPosition") {
    cls.push(styles.CardPortraitFullIndivid);
    clsContainer.push(styles.ContainerForFullIndivid);
  }
  if (portraitName == "Base") {
    cls.push(styles.CardPortraitBase);
    clsContainer.push(styles.ContainerForBase);
  }
  if (portraitName == "Alphabet") {
    cls.push(styles.CardPortraitAlphabet);
    clsContainer.push(styles.ContainerForAlphabet);
  }
  if (portraitName == "TwoPortrait") {
    cls.push(styles.CardTwoPortrait);
    clsContainer.push(styles.ContainerForTwoPortrait);
    if (!props.IsPrint) {
      cls.push(styles.CardForTwoPortraitNoPrint);
    }
  }

  return (
    <div className={clsWrapper.join(" ")}>
      <div className={cls.join(" ")}>
        <div id={props.Id} className={clsContainer.join(" ")}>
          {!!title ? (
            <div className="row">
              <div className="col-12 d-flex justify-content-center align-items-center">
                <h2 className="h4 text-light mb-1 mb-md-4 text-center">{title}</h2>
              </div>
            </div>
          ) : null}

          {props.children}
        </div>
      </div>
    </div>
  );
};
