import React, { Component } from "react";
import styles from "./TargetPowerShadow.module.scss";

class TargetPowerShadow2 extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
  }

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <div className={`${styles.ItemResContainer}`}>
        <div className={styles.ItemRes}>
          <div className={styles.ItemResTitle}>Цель</div>
          <div className={styles.ItemResValue}>{this.props.ValueTarget}</div>
        </div>
        <div className={`${styles.ItemRes} mx-3`}>
          <div className={styles.ItemResTitle}>Сила</div>
          <div className={styles.ItemResValue}>{this.props.ValuePower}</div>
        </div>
        <div className={styles.ItemRes}>
          <div className={styles.ItemResTitle}>Тень</div>
          <div className={styles.ItemResValue}>{this.props.ValueShadow}</div>
        </div>
        <div className={`${styles.ItemRes} ms-3`}>
          <div className={styles.ItemResTitle}>Главный аркан</div>
          <div className={styles.ItemResValue}>{this.props.MainArcane}</div>
        </div>
      </div>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default TargetPowerShadow2;
