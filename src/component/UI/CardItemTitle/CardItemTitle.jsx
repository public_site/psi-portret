import React, { Component } from "react";
import styles from "./CardItemTitle.module.scss";
import { Button, Modal } from "react-bootstrap";
import { BiRuble } from "react-icons/bi";
import { getDescPositionForIndividPortrait } from "../../../Func";
import ModalOrderTranscript from "../../ModalOrderTranscript/ModalOrderTranscript";

class CardItemTitle extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      IsShow: false,
      IShow2: false,
    };
  }
  handleShow = (e) => {
    this.setState({
      IsShow: true,
    });
  };

  handleClose = (e) => {
    this.setState({
      IsShow: false,
    });
  };

  handleShow2 = (isShow) => {
    this.setState({
      IsShow2: isShow,
    });
  };
  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var number = this.props.ValueNumber;
    var desc = this.props.ValueDesc;
    var descNumber = getDescPositionForIndividPortrait(number);
    var strNumber = "";
    if (!!parseInt(number)) {
      strNumber = "№ " + number;
    } else {
      strNumber = number;
    }
    var isTwoPortrait = !!this.props.IsTwoPortrait;

    var clsButton = [styles.ButtonLink];
    if (isTwoPortrait) {
      clsButton.push(styles.ButtonLinkTowPortrait);
    }

    return (
      <React.Fragment>
        {!!desc ? (
          <div className={clsButton.join(" ")} onClick={this.handleShow}>
            <span className={`me-1 ${styles.ValueNumber}`}>{strNumber}</span>
            <span className={`${styles.ValueDesc} hidden-md`}>{desc}</span>
          </div>
        ) : (
          <div className={clsButton.join(" ")}>
            <span className={`${styles.ValueNumber}`} onClick={this.handleShow}>
              {strNumber}
            </span>
          </div>
        )}
        <Modal show={this.state.IsShow} onHide={this.handleClose} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
          <Modal.Header closeButton>
            <Modal.Title>
              Позиция{" "}
              <span className="text-primary">
                <b>№{number}</b>
              </span>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p className="h5 m-0">{descNumber}</p>
            <hr />
            <div className="row">
              <div className="col-1 col-md-1 d-flex align-items-center">
                <span className={styles.Circle}>
                  <BiRuble />
                </span>
              </div>
              <div className="col-11 col-md-9 d-flex align-items-center">
                <p className="h-md-5 m-0 text-primary">
                  <b>Доступны подробные описания позиций полного портрета + формулы расчёта</b>
                </p>
              </div>
              <div className="col-12 col-md-2 d-flex justify-content-center justify-content-md-start align-items-center mt-2 mt-md-0">
                <a
                  href="#"
                  className="btn btn-primary text-light"
                  onClick={(e) => {
                    this.handleClose();
                    this.handleShow2(true);
                  }}
                >
                  Подробнее
                </a>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Modal>

        <ModalOrderTranscript IsShowModal={this.state.IsShow2} handleShowModal={this.handleShow2} />
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    //
  }
}

export default CardItemTitle;
