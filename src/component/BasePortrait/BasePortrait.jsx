import React, { Component } from "react";
import styles from "./BasePortrait.module.scss";
import CardItem from "../UI/CardItem/CardItem";
import { CardPortrait } from "../UI/CardPortrait/CardPortrait";
import ButtonsBottomControl from "../UI/ButtonsBottomControl/ButtonsBottomControl";
import TargetPowerShadow from "../UI/TargetPowerShadow/TargetPowerShadow";
import ButtonDownloadAndPrint from "../UI/ButtonDownloadAndPrint/ButtonDownloadAndPrint";
import { Button, Modal } from "react-bootstrap";
import { getSumArchetype } from "../../Func";

class BasePortrait extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);

    this.state = {
      IsPrint: false,
      PeriodStartCircle1: "0",
      PeriodEndCircle1: "",
      PeriodStartCircle2: "",
      PeriodEndCircle2: "",
      PeriodStartCircle3: "",
      PeriodEndCircle3: "...",
      Values: {
        ValuePosition1: "",
        ValueSupPosition1: "н",
        ValuePosition2: "",
        ValueSupPosition2: "н",
        ValuePosition3: "",
        ValueSupPosition3: "н",
        ValuePosition4: "",
        ValueSupPosition4: "",
        ValuePosition5: "",
        ValueSupPosition5: "",
        ValuePosition6: "",
        ValueSupPosition6: "",
        ValuePosition7: "",
        ValueSupPosition7: "",
        ValuePosition8: "",
        ValueSupPosition8: "",
      },
    };
  }

  /**
   * Перезагрузка страницы
   * @param {*} e
   */
  handleRefresh = (e) => {
    window.location.reload();
  };

  /**
   * Скачать и печать
   * @param {*} e Event
   */
  handlePrint = (e) => {
    var isPrint = this.state.IsPrint;
    if (!isPrint) {
      setTimeout(() => {
        window.print();
      }, 1000);
    }
    this.setState({
      IsPrint: !isPrint,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    var valueSumSupM = getSumArchetype("м", this.state.Values);
    var valueSumSupZh = getSumArchetype("ж", this.state.Values);
    var valueSumSupN = getSumArchetype("н", this.state.Values);

    var values = this.state.Values;
    var numberSelected = [values.ValuePower, values.ValueShadow, values.ValueTarget];

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 text-center">
            <h1 className="h3 text-uppercase my-1 my-md-3">Базовый портрет | Периоды Жизни</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <CardPortrait IsPrint={this.state.IsPrint} handlePrint={this.handlePrint} PortraitName="Base">
              <div className="row font-cambria">
                <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                  <div className="d-flex justify-content-center align-items-center">
                    <span className="h4 m-0 text-capitalize">{this.props.Name}</span>
                  </div>
                  <div className="d-flex justify-content-center align-items-center">
                    <span>Архетип имени: </span> <span className={`text-primary ms-1 ${styles.TextNameArchetype}`}>{this.props.NameArchetype}</span>
                  </div>
                  <div className="">
                    <TargetPowerShadow ValueTarget={values.ValueTarget} ValuePower={values.ValuePower} ValueShadow={values.ValueShadow} />
                  </div>
                  <div className="d-flex flex-column flex-md-row justify-content-center align-items-center font-cambria">
                    <div className="h4 my-3">
                      <span className="me-3">Дата:</span>
                      <span>{this.props.DateOfBirth}</span>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row font-cambria">
                <div className="col-12">
                  <div className="row">
                    <div className="col-4 text-center">
                      <div className={styles.TitleCycle}>цикл №1</div>
                      <div className="d-flex justify-content-center align-items-center">
                        <span className="px-1">|</span>
                        <span>{this.state.PeriodStartCircle1}</span>
                        <span>~</span>
                        <span>{this.state.PeriodEndCircle1}</span>
                        <span className="ps-1">лет</span>
                        <span className="px-1">|</span>
                      </div>
                    </div>
                    <div className="col-4 text-center">
                      <div className={styles.TitleCycle}>цикл №2</div>
                      <div className="d-flex justify-content-center align-items-center">
                        <span className="px-1">|</span>
                        <span>{this.state.PeriodStartCircle2}</span>
                        <span>~</span>
                        <span>{this.state.PeriodEndCircle2}</span>
                        <span className="ps-1">лет</span>
                        <span className="px-1">|</span>
                      </div>
                    </div>
                    <div className="col-4 text-center">
                      <div className={styles.TitleCycle}>цикл №3</div>
                      <div className="d-flex justify-content-center align-items-center">
                        <span className="px-1">|</span>
                        <span>{this.state.PeriodStartCircle3}</span>
                        <span>~</span>
                        <span>{this.state.PeriodEndCircle3}</span>
                        <span className="ps-1">лет</span>
                        <span className="px-1">|</span>
                      </div>
                    </div>
                  </div>
                  <hr className="my-2" />
                  <div className="row">
                    <div className="col-12">
                      <div className="row">
                        <div className="col-1"></div>
                        <div className="col-3 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition1) != -1 ? true : false}
                            ValueNumberTitle={`1`}
                            ValueDesc={`формирование\nличности`}
                            ValueNumber={values.ValuePosition1}
                            ValueSup={values.ValueSupPosition1}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-4 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition2) != -1 ? true : false}
                            ValueNumberTitle={`2`}
                            ValueDesc={`школа\nвзросления`}
                            ValueNumber={values.ValuePosition2}
                            ValueSup={values.ValueSupPosition2}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-3 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition3) != -1 ? true : false}
                            ValueNumberTitle={`3`}
                            ValueDesc={`экзаменатор,\nк чему приходим`}
                            ValueNumber={values.ValuePosition3}
                            ValueSup={values.ValueSupPosition3}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-1"></div>
                      </div>
                      <div className="row">
                        <div className="col-3"></div>
                        <div className="col-3 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition4) != -1 ? true : false}
                            ValueNumberTitle={`4`}
                            ValueNumber={values.ValuePosition4}
                            ValueSup={values.ValueSupPosition4}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-3 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition5) != -1 ? true : false}
                            ValueNumberTitle={`5`}
                            ValueNumber={values.ValuePosition5}
                            ValueSup={values.ValueSupPosition5}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-3 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition7) != -1 ? true : false}
                            ValueNumberTitle={`7`}
                            ValueNumber={values.ValuePosition7}
                            ValueSup={values.ValueSupPosition7}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-5"></div>
                        <div className="col-2 text-center">
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition6) != -1 ? true : false}
                            ValueNumberTitle={`6`}
                            ValueNumber={values.ValuePosition6}
                            ValueSup={values.ValueSupPosition6}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-5"></div>
                      </div>
                      <div className="row">
                        <div className="col-5"></div>
                        <div className={`col-2 text-center ${styles.RowBorderBottom}`}>
                          <CardItem
                            IsSelected={numberSelected.indexOf(values.ValuePosition8) != -1 ? true : false}
                            ValueNumberTitle={`8`}
                            ValueNumber={values.ValuePosition8}
                            ValueSup={values.ValueSupPosition8}
                            IsVisualTaro={this.props.IsVisualTaro}
                          />
                        </div>
                        <div className="col-5"></div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <div className={`d-flex flex-column flex-md-row justify-content-center align-content-center my-2 ${styles.FontArchetypeSize}`}>
                        <span className="me-md-2 d-flex justify-content-center align-items-center text-light">Архетипы:</span>
                        <div className="d-flex justify-content-center align-items-center">
                          <span className="mx-1">
                            {valueSumSupM > valueSumSupZh && valueSumSupM > valueSumSupN ? (
                              <span className="text-primary me-1">М-мужской</span>
                            ) : (
                              <span className="text-light me-1">М-мужской</span>
                            )}
                            <span className="text-primary">{valueSumSupM}</span>
                          </span>
                          <span className="text-light mx-1 hidden-md">|</span>
                          <span className="mx-1">
                            {valueSumSupZh > valueSumSupM && valueSumSupZh > valueSumSupN ? (
                              <span className="text-primary me-1">Ж-женский</span>
                            ) : (
                              <span className="text-light me-1">Ж-женский</span>
                            )}
                            <span className="text-primary">{valueSumSupZh}</span>
                            <span className="text-light mx-1 hidden-md">|</span>
                          </span>
                          <span className="mx-1">
                            {valueSumSupN > valueSumSupM && valueSumSupN > valueSumSupZh ? (
                              <span className="text-primary me-1">Н-нейтральный</span>
                            ) : (
                              <span className="text-light me-1">Н-нейтральный</span>
                            )}
                            <span className="text-primary">{valueSumSupN}</span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {false ? (
                <div className="row">
                  <div className="col-12 d-flex flex-column justify-content-center align-items-center">
                    <button className={`btn btn-primary my-1 text-light ${styles.ButtonSubmit}`} onClick={(e) => this.handleShowModal2(true)}>
                      Краткая расшифровка позиций Базового Портрета
                    </button>
                    <ButtonDownloadAndPrint handlePrint={this.handlePrint} />
                  </div>
                </div>
              ) : null}
            </CardPortrait>
            <ButtonsBottomControl
              ButtonLeftText="назад к вводу даты"
              ButtonCenter="подробности к портрету"
              ButtonRight="композитный портрет"
              onButtonLeft={(e) => this.props.onChangeAction("Form")}
              onButtonCenter={(e) => this.handleShowModal1(true)}
              ButtonRightUrl={"/calc-numerologist-luna-shafran/composite-portrait"}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {
    var dtString = this.props.DateOfBirth + "";
    var periodEndCircle1 = 0;
    for (let i = 0; i < dtString.length; i++) {
      var parsed = parseInt(dtString[i]);
      if (!isNaN(parsed)) {
        periodEndCircle1 += parsed;
      }
    }
    var periodStartCircle2 = periodEndCircle1 + 1;
    var periodEndCircle2 = periodStartCircle2 + 8;
    var periodStartCircle3 = periodEndCircle2 + 1;
    this.setState({
      PeriodEndCircle1: periodEndCircle1,
      PeriodStartCircle2: periodStartCircle2,
      PeriodEndCircle2: periodEndCircle2,
      PeriodStartCircle3: periodStartCircle3,
      Values: this.props.Values,
    });
  }
}

export default BasePortrait;
