import React, { Component } from "react";
import EntryForm from "../../component/forms/IndividPortraitForm/IndividPortraitForm";
import BasePortrait from "../../component/BasePortrait/BasePortrait";
import FullIndividPortrait from "../../component/FullIndividPortrait/FullIndividPortrait";
import AlphabetPortrait from "../../component/forms/AlphabetPortraitForm/AlphabetPortraitForm";
import { connect } from "react-redux";
import { PortraitAction } from "../../store/actions/PortraitAction";
import { validDate, convert22, convert10, convertToRoman, getArchetype, getNameArchetype, getPositionCharInAlphabet, calcAlphabet, calcIndividPortrait } from "../../Func";

class HomePage extends Component {
  state = {};

  /**
   * Создание компонента
   */
  constructor(props) {
    super(props);
    this.state = {
      Action: "Form",
      PreAction: "FullIndividPortrait",
      NameArchetype: "",
      IsVisualTaro: false,
      IsSuccess: false,
      DataForm: {
        InputDate: {
          Value: "", // 23.05.1977
          Validate: true,
        },
        InputName: {
          Value: "", // Луна
          Validate: true,
        },
      },
      Values: {},
    };
  }

  /**
   * Ввод даты
   * @param {*} e
   */
  onChangeDate = (e) => {
    var dataForm = this.state.DataForm;
    dataForm.InputDate.Value = e.target.value;
    dataForm.InputDate.Validate = validDate(e.target.value) ? true : false;
    this.setState({
      DataForm: dataForm,
    });
  };
  /**
   * Ввод имени
   * @param {*} e
   */
  onChangeName = (e) => {
    var dataForm = this.state.DataForm;
    var name = e.target.value;
    dataForm.InputName.Value = name;
    dataForm.InputName.Validate = name === "" ? false : true;
    var nameArchetype = getNameArchetype(this.state.DataForm.InputName.Value, true);
    this.setState({
      DataForm: dataForm,
      NameArchetype: nameArchetype,
    });
  };

  /**
   * Рассчитать дату
   * @param {*} e
   */
  handleSubmitCalculation = (e) => {
    e.preventDefault();
    var isSuccess = true;
    var dataForm = this.state.DataForm;
    var dtString = this.state.DataForm.InputDate.Value + "";
    var name = this.state.DataForm.InputName.Value + "";

    dataForm.InputName.Validate = name == "" ? false : true;
    dataForm.InputDate.Validate = validDate(dtString) ? true : false;
    if (!dataForm.InputName.Validate || !dataForm.InputDate.Validate) {
      isSuccess = false;
    }

    if (!dataForm.InputName.Validate || !dataForm.InputDate.Validate) {
      isSuccess = false;
    }
    if (!isSuccess) {
      this.setState({
        IsSuccess: isSuccess,
        DataForm: dataForm,
      });
      return;
    }

    var dtString = this.state.DataForm.InputDate.Value + "";
    var name = this.state.DataForm.InputName.Value + "";

    console.log({ dtString, name });

    var values = {};
    values = calcAlphabet(name, values);
    values = calcIndividPortrait(dtString, values);

    var action = this.state.PreAction;
    this.setState({
      Action: action,
      IsSuccess: true,
      Values: values,
    });
  };

  /**
   * Визуал в Таро
   * @param {*} e
   */
  onChangeCheckboxCardTaro = (e) => {
    var isVisualTaro = !this.state.IsVisualTaro;
    this.setState({
      IsVisualTaro: isVisualTaro,
    });
  };

  /**
   * Смена компонента/формы/экшена на странице
   * @param {string} nameAction Имя компонента/формы/экшена
   */
  onChangeAction = (nameAction) => {
    this.setState({
      Action: nameAction,
    });
  };
  /**
   * Предыдущий компонента/форма/экшен
   * @param {string} namePreAction Имя компонента/формы/экшена
   */
  onChangePreAction = (namePreAction) => {
    this.setState({
      PreAction: namePreAction,
    });
  };

  /**
   * Отрисовка компонента
   * @returns HTML Render
   */
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            {this.state.Action === "Form" ? (
              <EntryForm
                DataForm={this.state.DataForm}
                NameArchetype={this.state.NameArchetype}
                IsVisualTaro={this.state.IsVisualTaro}
                onChangeDate={(e) => this.onChangeDate(e)}
                onChangeName={(e) => this.onChangeName(e)}
                handleSubmitCalculation={(e) => this.handleSubmitCalculation(e)}
                onChangeCheckboxCardTaro={(e) => this.onChangeCheckboxCardTaro(e)}
                PreAction={this.state.PreAction}
                IsSuccess={this.state.IsSuccess}
                onChangePreAction={this.onChangePreAction}
              />
            ) : null}
            {this.state.Action === "FullIndividPortrait" ? (
              <FullIndividPortrait
                DateOfBirth={this.state.DataForm.InputDate.Value}
                Name={this.state.DataForm.InputName.Value}
                NameArchetype={this.state.NameArchetype}
                onChangeAction={(e) => this.onChangeAction(e)}
                Values={this.state.Values}
                IsVisualTaro={this.state.IsVisualTaro}
              />
            ) : null}
            {this.state.Action === "BasePortrait" ? (
              <BasePortrait
                DateOfBirth={this.state.DataForm.InputDate.Value}
                Name={this.state.DataForm.InputName.Value}
                NameArchetype={this.state.NameArchetype}
                onChangeAction={(e) => this.onChangeAction(e)}
                Values={this.state.Values}
                IsVisualTaro={this.state.IsVisualTaro}
              />
            ) : null}
            {this.state.Action === "AlphabetPortrait" ? <AlphabetPortrait Name={this.state.Name} /> : null}
          </div>
        </div>
      </React.Fragment>
    );
  }

  /**
   * Выполняется после отрисовки компонента
   */
  componentDidMount() {}
}

/**
 * Загрузка данных из Redux
 * @param {*} state
 * @returns
 */
function mapStateToProps(state) {
  let namePortrait = state.portrait.namePortrait;
  return {
    NamePortrait: namePortrait,
  };
}

/**
 * Загрузка данных в Redux
 * @param {*} dispatch
 * @returns
 */
function mapDispatchToProps(dispatch) {
  return {
    setPortraitAction: (namePortrait) => dispatch(PortraitAction(namePortrait)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
